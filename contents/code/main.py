from PyKDE4 import plasmascript
from PyKDE4.plasma import Plasma
from PyKDE4.kdeui import KIcon, KMessageBox
import mysql.connector
from subprocess import call
 
class MsgBoxRunner(plasmascript.Runner):
 
    def init(self):
        # called upon creation to let us run any intialization
        # tell the user how to use this runner
        self.addSyntax(Plasma.RunnerSyntax("pan :q:", "Display :q: in a messagebox"))
 
    def match(self, context):
        # called by krunner to let us add actions for the user
        if not context.isValid():
            return
 
        q = context.query()
 
        # look for our keyword 'msg'
        if not q.startsWith("pan "):
             return
 
        # ignore less than 3 characters (in addition to the keyword)
        if q.length() < 7:
            return
 
        # strip the keyword and leading space
        q = q[3:]
        q = q.trimmed()
 
        # now create an action for the user, and send it to krunner
        cnx = mysql.connector.connect(user='root',database='pancake',password='')
        cursor = cnx.cursor()
        cursor.execute("SELECT last_name,email,first_name,company,address,phone FROM pancake_clients WHERE last_name LIKE '%"+q+"%' OR email LIKE '%"+q+"%' OR first_name LIKE '%"+q+"%' OR company LIKE '%"+q+"%' OR address LIKE '%"+q+"%'");
        for row in cursor.fetchall():
            m = Plasma.QueryMatch(self.runner)
            if not row[0]:
                row[0] = 'n/a'
            if not row[1]:
                row[1] = 'n/a'
            if not row[2]:
                row[2] = 'n/a'
            if not row[3]:
                row[3] = 'n/a'
            if not row[4]:
                row[4] = 'n/a'
            if not row[5]:
                row[5] = 'n/a'
            m.setText("Contact: {0}, {1}: {2}\n {3}\n {4}\n {5}".format(row[0].encode("utf-8"), row[2].encode("utf-8"), row[1].encode("utf-8"), row[3].encode("utf-8"), row[4].encode("utf-8"), row[5].encode("utf-8")))
            m.setType(Plasma.QueryMatch.ExactMatch)
            m.setIcon(KIcon("dialog-information"))
            m.setData(row[1])
            context.addMatch(row[1], m)
        cursor.close()
 
    def run(self, context, match):
        # called by KRunner when the user selects our action,        
        # so lets keep our promise
        call("xdg-open mailto:{0}".format(match.data().toString()), shell=True)
        # KMessageBox.messageBox(None, KMessageBox.Information, match.data().toString())
 
 
def CreateRunner(parent):
    # called by krunner, must simply return an instance of the runner object
    return MsgBoxRunner(parent)